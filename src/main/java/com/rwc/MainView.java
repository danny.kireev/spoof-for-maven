package com.rwc;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Logger;
import java.lang.Process;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@SpringView(name = "")
public class MainView extends CustomComponent implements View {
    private static final Logger log = Logger.getLogger(MainView.class.getName());

    public static final String VIEW_NAME = "";

    public MainView() {
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
