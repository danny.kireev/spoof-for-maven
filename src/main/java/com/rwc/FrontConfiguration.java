package com.rwc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.util.logging.Logger;

@Configuration
@PropertySource("classpath:application.properties")
public class FrontConfiguration {
    private static final Logger log = Logger.getLogger(FrontConfiguration.class.getName());

    @Autowired
    private Environment env;

    @Bean(name = "baseModelsPath")
    public String baseModelsPath() {
        return env.getProperty("baseModelsPath");
    }

    @Bean(name = "viewerUrl")
    public String viewerUrl() {
        return env.getProperty("viewerUrl");
    }

    @Bean(name = "analyzeCommand")
    public String analyzeCommand() {
        return env.getProperty("analyzeCommand");
    }

    @Bean(name = "uploadFolder")
    public String uploadFolder() {
        return env.getProperty("uploadFolder");
    }

    @Bean(name = "rlUrl")
    public String rlUrl() {
        return env.getProperty("rlUrl");
    }

    @Bean(name = "analyzerLog")
    public String analyzerLog() {
	return env.getProperty("analyzerLog");
    }

}
