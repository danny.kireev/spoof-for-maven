package com.rwc;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@SpringBootApplication
public class FrontApplication {
	private static final Logger log = Logger.getLogger(FrontApplication.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(FrontApplication.class, args);
	}
}
