package com.rwc.js;

import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.AbstractJavaScriptComponent;

@JavaScript({"scene.js", "x3dom.js"})
public class SceneComponent extends AbstractJavaScriptComponent {

}
